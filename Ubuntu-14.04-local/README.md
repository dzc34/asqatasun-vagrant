# Asqatasun Box on Ubuntu 14.04 with local build

## 0. Place local Asqatasun binary

Locate the Asqatasun binary (`asqatasun*.tar.gz`) you've just built. Copy it into this directory (containing the vagrantFile).
Symlinks do not work, thus a copy is mandatory.

## 1. Outsite the box

Get into this directory, then:

```
vagrant up
vagrant ssh
```

## 2. Inside the box

```
sudo -i
cd /vagrant
./asqatasun.sh
```

The script ends with a `tail -f` on Asqatasun and Tomcat log files

## 3. Destroy the box

From outsite the box:

```
vagrant destroy -f
```